﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PartyInvites.Models
{
    public static class Repository
    {
        private static List<LoginModel> logins = new List<LoginModel>();
        public static IEnumerable<LoginModel> Logins=> logins; 

        public static void AddLogin(LoginModel login) { logins.Add(login); }
    }
}
