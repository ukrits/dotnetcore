﻿using PartyInvites.Models;
using Xunit;

namespace PartyInvitesXUnitTest
{
    public class ProductTest
    {
        [Fact]
        public void CanChangeProductName() {
            var p = new Product { Name = "Test", Price = 100M };
            //Act
            p.Name = "New Name";

            //Assert
            Assert.Equal("New Name",p.Name);
        }

        [Fact]
        public void CanChangeProductPrice()
        {
            var p = new Product { Name = "Test", Price = 100M };
            //Act
            p.Price = 200M;

            //Assert
            Assert.Equal(200M, p.Price);
        }
    }
}
