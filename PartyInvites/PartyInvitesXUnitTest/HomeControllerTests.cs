﻿using PartyInvites.Controllers;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using PartyInvites.Models;
using System.Collections.Generic;
using Moq;

namespace PartyInvitesXUnitTest
{
    public class HomeControllerTests
    {
        //class ModelCompleteFakeRepository : IRepository
        //{
        //    public IEnumerable<Product> Products { get; set; } 
        //    //IEnumerable<Product> IRepository.Products { get; } = new Product[] { new Product { Name = "P1",Price = 275M }
        //    //                                                                     ,new Product { Name = "P2",Price = 48.95M }
        //    //                                                                     ,new Product { Name = "P3",Price =20M }
        //    //                                                                     ,new Product { Name = "P3",Price =34.95M } };
            
        //    public void AddProduct(Product p)
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //}

        //class ModelCompleteFakeRepositoryPriceUnder50 : IRepository
        //{
        //    IEnumerable<Product> IRepository.Products { get; } = new Product[] { new Product { Name = "P1",Price = 5M }
        //                                                                         ,new Product { Name = "P2",Price = 48.95M }
        //                                                                         ,new Product { Name = "P3",Price =20M }
        //                                                                         ,new Product { Name = "P3",Price =34.95M } };

        //    void IRepository.AddProduct(Product p)
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //}


        //[Theory]
        //[InlineData(275, 48.95, 20, 42.95)]
        //[InlineData(5, 48.95, 20, 42.95)]
        //public void IndexActionModelIsComplete(decimal price1, decimal price2, decimal price3, decimal price4) {
        //    //Arrange
        //    var ctrl = new HomeController();
        //    ctrl.Repository = new ModelCompleteFakeRepository {
        //        Products = new Product[] { new Product { Name = "P1",Price = price1 }
        //                                                                         ,new Product { Name = "P2",Price = price2 }
        //                                                                         ,new Product { Name = "P3",Price = price3 }
        //                                                                         ,new Product { Name = "P3",Price = price4 }, }
        //        };
        //    //Act
        //    var model = (ctrl.Index() as ViewResult)?.ViewData.Model as IEnumerable<Product>;
        //    //Assert 
        //    Assert.Equal(ctrl.Repository.Products, model, Comparer.Get<Product>((p1, p2) => p1.Name == p2.Name && p1.Price == p2.Price));
        //}

        [Theory]
        [ClassData(typeof(ProductTestData))]
        public void IndexActionModelIsComplete(Product[] products)
        {
            //Arrange
            var mock = new Mock<IRepository>();
            mock.SetupGet(m => m.Products).Returns(products);

            var ctrl = new HomeController { Repository = mock.Object };
            //Act
            var model = (ctrl.Index() as ViewResult)?.ViewData.Model as IEnumerable<Product>;
            //Assert 
            Assert.Equal(ctrl.Repository.Products, model, Comparer.Get<Product>((p1, p2) => p1.Name == p2.Name && p1.Price == p2.Price));
        }

        //class PropertyOnceFakeRepository : IRepository {
        //    public int PropertyCounter { get; set; } = 0;

        //    public IEnumerable<Product> Products { get {
        //            PropertyCounter++;
        //            return new[] { new Product { Name = "P1", Price = 100 } };
        //                } }

        //    public void AddProduct(Product p)
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //}

        [Fact]
        public void RepositoryPropertyCalledOnce()
        {
            //Arrange
            var mock = new Mock<IRepository>();
            mock.SetupGet(m => m.Products).Returns(new[] { new Product { Name = "P1", Price = 100 } });
            
            var ctrl = new HomeController { Repository = mock.Object };
            var result = ctrl.Index();
            mock.VerifyGet(m => m.Products,Times.Once);
            // Assert.Equal(1, repo.PropertyCounter);
        }
    }
}
